package com.fedcorp.file_manager;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class View {
private LinkedHashMap<Integer, Launcher> options;
private ArrayList<String> paths;

    public LinkedHashMap<Integer, Launcher> getOptionsMap() {
        return options;
    }

    public ArrayList<String> getPathsList() {
        return paths;
    }

    public void display(String path) {
        File file = new File(path);
        options = new LinkedHashMap<>();
        paths = new ArrayList<>();
        paths.add(path);
        if (file.exists()) {
            try {
                if (file.isDirectory()) {
                    File[] files = file.listFiles();
                    int count = 0;
                    for (File f : files) {
                        System.out.print(++count + " ");
                        if (f.isDirectory()) {
                            System.out.println("/" + f.getName() + "/");
                            options.put(count, Controller::execute);
                            paths.add(f.getAbsolutePath());
                        } else {
                            System.out.println(f.getName());
                            options.put(count, this::showFileMenu);
                            paths.add(f.getAbsolutePath());
                        }
                    }
                    System.out.println();
                    System.out.println(++count + " Create new directory");
                    options.put(count, Controller::createNewDir);
                    paths.add(file.getAbsolutePath());

                    System.out.println(++count + " Create new file");
                    options.put(count, Controller::createNewFile);
                    paths.add(file.getAbsolutePath());

                    System.out.println(++count + " Change directory");
                    options.put(count, Controller::changeDir);
                    paths.add(file.getAbsolutePath()); //Використовується лише щоб зберегти порядок в списку

                    if(file.getParent()!=null){
                        System.out.println(++count + " ../ <- to previos directory");
                        options.put(count, Controller::execute);
                        paths.add(file.getParentFile().getAbsolutePath());
                    }

                    System.out.println();
                    System.out.println(++count + " Exit from program");
                    options.put(count, Controller::exit);
                    paths.add(file.getAbsolutePath()); //Використовується лише щоб зберегти порядок в списку
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public void showFileMenu(String path){}
    public void showNewDirView(String path){}
}
