package com.fedcorp.file_manager;

@FunctionalInterface
public interface Launcher {
    void launch(String s);
}
