package com.fedcorp.file_manager;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Controller {

    public static void execute(String s){
        Scanner sc = new Scanner(System.in);
        View view = new View();
        view.display(s);
        System.out.println("Виберіть дії");
        int action = sc.nextInt();
        view.getOptionsMap().get(action).launch(view.getPathsList().get(action));

    }
    public static void createNewFile(String path){
        System.out.println("Введіть назву файлу.");
        Scanner sc = new Scanner(System.in);
        File f = new File(path+"\\"+ sc.nextLine());
        if(!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            System.out.println("Файл з таким іменем вже існує!");
        }
        execute(path);
    }

    public static void createNewDir(String path){
        System.out.println("Введіть назву директорії.");
        Scanner sc = new Scanner(System.in);
        File f = new File(path+"\\"+ sc.nextLine());
        if(!f.exists()) {
            f.mkdir();
        }else{
            System.out.println("Директорія вже існує");
        }
        execute(path);
    }

    public static void changeDir(String path){
        System.out.println("Введіть адресу директорії.");
        Scanner sc = new Scanner(System.in);
        File f = new File(sc.nextLine());
        if(f.exists()) {
            execute(f.getAbsolutePath());
        }else {
            System.out.println("Невірно вказаний шлях, спробуйте ще раз!");
            execute(path);
        }
    }
    public static void exit(String s){
        System.out.println("Ви вийшли з програми");
    }
}
