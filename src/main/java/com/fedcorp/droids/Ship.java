package com.fedcorp.droids;

import java.io.Serializable;
import java.util.LinkedList;

public class Ship implements Serializable {

    private LinkedList<Droid> ship;
    private transient SecretPlan plan = new SecretPlan();

    public Ship() {
        ship = new LinkedList<>();
    }

    public boolean addDroid(Droid d){
        return ship.add(d);
    }

    public Droid getDroid(){
        return ship.poll();
    }
}
