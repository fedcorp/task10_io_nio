package com.fedcorp.droids.save_and_load;

import java.io.*;

public class Serializer {

    public void serialize(Object o){
        File file = new File(System.getProperty("user.dir") + "\\src\\main\\resources\\Save.txt");

        try (ObjectOutputStream oOut = new ObjectOutputStream(new FileOutputStream(file))) {
            oOut.writeObject(o);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public Object unSerialize(){
        File file = new File(System.getProperty("user.dir") + "\\src\\main\\resources\\Save.txt");

        try (ObjectInputStream oOut = new ObjectInputStream(new FileInputStream(file))) {
            return oOut.readObject();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
