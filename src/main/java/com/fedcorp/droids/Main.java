package com.fedcorp.droids;

import com.fedcorp.droids.save_and_load.Serializer;

public class Main {
    public static void main(String[] args) {

        Serializer s = new Serializer();
        Ship ship = new Ship();

        ship.addDroid(new Droid(800, 10));
        ship.addDroid(new Droid(90, 10));
        ship.addDroid(new Droid(110, 15));
        ship.addDroid(new Droid(105, 11));

        s.serialize(ship);
        Ship recoveredShip = (Ship)s.unSerialize();
        System.out.println(recoveredShip.getDroid().getHP() + " " + recoveredShip.getDroid().getHP());
    }
}
