package com.fedcorp.droids;

import java.io.Serializable;

public class Droid implements Serializable {

    private int HP;
    private int AttackPW;

    public Droid(int HP, int attackPW) {
        this.HP = HP;
        AttackPW = attackPW;
    }

    public int getHP() {
        return HP;
    }

    public int getAttackPW() {
        return AttackPW;
    }
}
