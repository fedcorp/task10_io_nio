package com.fedcorp.file_reader;

import java.io.*;
import java.util.Date;

public class FileReadingComparator {

    public void fileReaderDefault(){
        StringBuilder string = new StringBuilder();
        File file = new File(System.getProperty("user.dir") + "\\src\\main\\resources\\Khorstmann_Java_.pdf");
        try(FileReader br =  new FileReader(file)) {
            String s;
            int data = br.read();
            long timeBefore = new Date().getTime();
            while( data !=-1){
                string.append(((char)data));
                data = br.read();
            }
            long timeAfter = new Date().getTime();
            System.out.print("Метод fileReaderDefault виконано. ");
            System.out.println("Зчитування тривало " + (timeAfter-timeBefore) + "мс.");
            System.out.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void bufferedReaderDefault(){
        StringBuilder string = new StringBuilder();
        File file = new File(System.getProperty("user.dir") + "\\src\\main\\resources\\Khorstmann_Java_.pdf");
        try(BufferedReader br = new BufferedReader( new FileReader(file))) {
            String s;
            long timeBefore = new Date().getTime();
            while((s = br.readLine()) !=null){
                string.append(s);
            }
            long timeAfter = new Date().getTime();
            System.out.print("Метод bufferedReaderDefault виконано. ");
            System.out.println("Зчитування тривало " + (timeAfter-timeBefore) + "мс.");
            System.out.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void bufferedReader_1MB(){
        StringBuilder string = new StringBuilder();
        File file = new File(System.getProperty("user.dir") + "\\src\\main\\resources\\Khorstmann_Java_.pdf");
        try(BufferedReader br = new BufferedReader( new FileReader(file), 1024)) {
            String s;
            long timeBefore = new Date().getTime();
            while((s = br.readLine()) !=null){
                string.append(s);
            }
            long timeAfter = new Date().getTime();
            System.out.print("Метод bufferedReader_1MB виконано. ");
            System.out.println("Зчитування тривало " + (timeAfter-timeBefore) + "мс.");
            System.out.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void bufferedReader_2MB(){
        StringBuilder string = new StringBuilder();
        File file = new File(System.getProperty("user.dir") + "\\src\\main\\resources\\Khorstmann_Java_.pdf");
        try(BufferedReader br = new BufferedReader( new FileReader(file), 2048)) {
            String s;
            long timeBefore = new Date().getTime();
            while((s = br.readLine()) !=null){
                string.append(s);
            }
            long timeAfter = new Date().getTime();
            System.out.print("Метод bufferedReader_2MB виконано. ");
            System.out.println("Зчитування тривало " + (timeAfter-timeBefore) + "мс.");
            System.out.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void bufferedReader_8MB(){
        StringBuilder string = new StringBuilder();
        File file = new File(System.getProperty("user.dir") + "\\src\\main\\resources\\Khorstmann_Java_.pdf");
        try(BufferedReader br = new BufferedReader( new FileReader(file), 1024*8)) {
            String s;
            long timeBefore = new Date().getTime();
            while((s = br.readLine()) !=null){
                string.append(s);
            }
            long timeAfter = new Date().getTime();
            System.out.print("Метод bufferedReader_2MB виконано. ");
            System.out.println("Зчитування тривало " + (timeAfter-timeBefore) + "мс.");
            System.out.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void bufferedReaderWithInputStreamReader(){
        StringBuilder string = new StringBuilder();
        File file = new File(System.getProperty("user.dir") + "\\src\\main\\resources\\Khorstmann_Java_.pdf");
        try (BufferedReader bf = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"))) {
            String line;
            long timeBefore = new Date().getTime();
            while ((line = bf.readLine()) != null) {
                string.append(line);
            }
            long timeAfter = new Date().getTime();
            System.out.print("Метод bufferedReaderWithInputStreamReader виконано. ");
            System.out.println("Зчитування тривало " + (timeAfter-timeBefore) + "мс.");
            System.out.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
