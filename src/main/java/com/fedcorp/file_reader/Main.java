package com.fedcorp.file_reader;

public class Main {
    public static void main(String[] args) {

        FileReadingComparator f = new FileReadingComparator();

        f.bufferedReaderDefault();
        f.fileReaderDefault();
        f.bufferedReaderWithInputStreamReader();
        f.bufferedReader_1MB();
        f.bufferedReader_2MB();
        f.bufferedReader_8MB();
    }
}
