package com.fedcorp.comments_reader;

import java.io.*;

public class Parser {
    public String parseComments(String name) {
        //Потрібно виключити
        StringBuilder string = new StringBuilder();
        File file = new File(System.getProperty("user.dir") + "\\src\\main\\java\\com\\fedcorp\\comments_reader\\" + name + ".java");
        if (file.isFile()) {
            try (BufferedReader bf = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"))) {
                String line;
                boolean commentStarted = false;
                while ((line = bf.readLine()) != null) {
                    if (!commentStarted) {
                        //Перевіряємо чи містить стрічка обидва типи початку коментарів
                        if (line.contains("//") && (line.contains("/*"))) {
                            //якщо так, то визначаємо який коментар був відкритий першим
                            if (line.indexOf("//") < line.indexOf("/*")) {
                                string.append(line.substring(line.indexOf("//")));
                                string.append("\n");
                            } else {
                                if (!line.contains("*/")) {
                                    commentStarted = true;
                                    string.append(line.substring(line.indexOf("/*")));
                                    string.append("\n");
                                } else {
                                    string.append(line.substring(line.indexOf("/*"), line.indexOf("*/", line.indexOf("/*")) + 2));
                                    string.append("\n");
                                }
                            }
                        } else {
                            if (line.contains("//")) {
                                string.append(line.substring(line.indexOf("//")));
                                string.append("\n");
                            }

                            if (line.contains("/*")) {
                                if (!line.contains("*/")) {
                                    commentStarted = true;
                                    string.append(line.substring(line.indexOf("/*")));
                                    string.append("\n");
                                } else {
                                    string.append(line.substring(line.indexOf("/*"), line.indexOf("*/", line.indexOf("/*")) + 2));
                                    string.append("\n");
                                }
                            }
                        }
                    } else {
                        if (line.contains("*/")) {
                            commentStarted = false;
                            string.append((line.substring(0, line.indexOf("*/") + 2)).trim());
                            string.append("\n");
                        } else {
                            string.append(line.trim());
                            string.append("\n");
                        }
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return string.toString();
        } else {
            return "Файл з даною назвою не існує!";
        }
    }
}
