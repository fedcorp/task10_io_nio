package com.fedcorp.comments_reader;

import java.util.Scanner;

/**
 * @Autor - Олег Федорів
 */
public class Main {
    //Дані коментарі повинні зчитуватись парсером
    public static void main(String[] args) {
        Parser parser = new Parser();
        Scanner sc = new Scanner(System.in);
        System.out.println("Введіть назву файлу з директорії 'comments_reader'");
        String tmp = sc.nextLine();
        System.out.println(parser.parseComments(tmp));
    }
    /*
     *  В середині коментаря може міститись подвійний слеш //
     * */
}
